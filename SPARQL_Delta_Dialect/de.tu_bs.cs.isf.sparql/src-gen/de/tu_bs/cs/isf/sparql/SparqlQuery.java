/**
 */
package de.tu_bs.cs.isf.sparql;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Query</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link de.tu_bs.cs.isf.sparql.SparqlQuery#getPrefixes <em>Prefixes</em>}</li>
 *   <li>{@link de.tu_bs.cs.isf.sparql.SparqlQuery#getBlocks <em>Blocks</em>}</li>
 * </ul>
 *
 * @see de.tu_bs.cs.isf.sparql.SparqlPackage#getSparqlQuery()
 * @model
 * @generated
 */
public interface SparqlQuery extends EObject {
	/**
	 * Returns the value of the '<em><b>Prefixes</b></em>' reference list.
	 * The list contents are of type {@link de.tu_bs.cs.isf.sparql.Prefix}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Prefixes</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Prefixes</em>' reference list.
	 * @see de.tu_bs.cs.isf.sparql.SparqlPackage#getSparqlQuery_Prefixes()
	 * @model
	 * @generated
	 */
	EList<Prefix> getPrefixes();

	/**
	 * Returns the value of the '<em><b>Blocks</b></em>' reference list.
	 * The list contents are of type {@link de.tu_bs.cs.isf.sparql.Block}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Blocks</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Blocks</em>' reference list.
	 * @see de.tu_bs.cs.isf.sparql.SparqlPackage#getSparqlQuery_Blocks()
	 * @model
	 * @generated
	 */
	EList<Block> getBlocks();

} // SparqlQuery
