/**
 */
package de.tu_bs.cs.isf.sparql;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Block</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link de.tu_bs.cs.isf.sparql.Block#getEntries <em>Entries</em>}</li>
 * </ul>
 *
 * @see de.tu_bs.cs.isf.sparql.SparqlPackage#getBlock()
 * @model abstract="true"
 * @generated
 */
public interface Block extends EObject {
	/**
	 * Returns the value of the '<em><b>Entries</b></em>' reference list.
	 * The list contents are of type {@link de.tu_bs.cs.isf.sparql.Entry}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Entries</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Entries</em>' reference list.
	 * @see de.tu_bs.cs.isf.sparql.SparqlPackage#getBlock_Entries()
	 * @model
	 * @generated
	 */
	EList<Entry> getEntries();

} // Block
