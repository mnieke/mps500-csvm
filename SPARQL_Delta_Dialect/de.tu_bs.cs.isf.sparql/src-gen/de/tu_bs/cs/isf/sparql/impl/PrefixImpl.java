/**
 */
package de.tu_bs.cs.isf.sparql.impl;

import de.tu_bs.cs.isf.sparql.Prefix;
import de.tu_bs.cs.isf.sparql.SparqlPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Prefix</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class PrefixImpl extends EntryImpl implements Prefix {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected PrefixImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return SparqlPackage.Literals.PREFIX;
	}

} //PrefixImpl
