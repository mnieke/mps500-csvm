/**
 */
package de.tu_bs.cs.isf.sparql.impl;

import de.tu_bs.cs.isf.sparql.Block;
import de.tu_bs.cs.isf.sparql.Prefix;
import de.tu_bs.cs.isf.sparql.SparqlPackage;
import de.tu_bs.cs.isf.sparql.SparqlQuery;

import java.util.Collection;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectResolvingEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Query</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link de.tu_bs.cs.isf.sparql.impl.SparqlQueryImpl#getPrefixes <em>Prefixes</em>}</li>
 *   <li>{@link de.tu_bs.cs.isf.sparql.impl.SparqlQueryImpl#getBlocks <em>Blocks</em>}</li>
 * </ul>
 *
 * @generated
 */
public class SparqlQueryImpl extends MinimalEObjectImpl.Container implements SparqlQuery {
	/**
	 * The cached value of the '{@link #getPrefixes() <em>Prefixes</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPrefixes()
	 * @generated
	 * @ordered
	 */
	protected EList<Prefix> prefixes;

	/**
	 * The cached value of the '{@link #getBlocks() <em>Blocks</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBlocks()
	 * @generated
	 * @ordered
	 */
	protected EList<Block> blocks;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected SparqlQueryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return SparqlPackage.Literals.SPARQL_QUERY;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Prefix> getPrefixes() {
		if (prefixes == null) {
			prefixes = new EObjectResolvingEList<Prefix>(Prefix.class, this, SparqlPackage.SPARQL_QUERY__PREFIXES);
		}
		return prefixes;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Block> getBlocks() {
		if (blocks == null) {
			blocks = new EObjectResolvingEList<Block>(Block.class, this, SparqlPackage.SPARQL_QUERY__BLOCKS);
		}
		return blocks;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case SparqlPackage.SPARQL_QUERY__PREFIXES:
				return getPrefixes();
			case SparqlPackage.SPARQL_QUERY__BLOCKS:
				return getBlocks();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case SparqlPackage.SPARQL_QUERY__PREFIXES:
				getPrefixes().clear();
				getPrefixes().addAll((Collection<? extends Prefix>)newValue);
				return;
			case SparqlPackage.SPARQL_QUERY__BLOCKS:
				getBlocks().clear();
				getBlocks().addAll((Collection<? extends Block>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case SparqlPackage.SPARQL_QUERY__PREFIXES:
				getPrefixes().clear();
				return;
			case SparqlPackage.SPARQL_QUERY__BLOCKS:
				getBlocks().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case SparqlPackage.SPARQL_QUERY__PREFIXES:
				return prefixes != null && !prefixes.isEmpty();
			case SparqlPackage.SPARQL_QUERY__BLOCKS:
				return blocks != null && !blocks.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //SparqlQueryImpl
