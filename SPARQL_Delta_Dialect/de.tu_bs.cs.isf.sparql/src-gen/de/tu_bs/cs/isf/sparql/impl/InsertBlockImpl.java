/**
 */
package de.tu_bs.cs.isf.sparql.impl;

import de.tu_bs.cs.isf.sparql.InsertBlock;
import de.tu_bs.cs.isf.sparql.SparqlPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Insert Block</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class InsertBlockImpl extends BlockImpl implements InsertBlock {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected InsertBlockImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return SparqlPackage.Literals.INSERT_BLOCK;
	}

} //InsertBlockImpl
