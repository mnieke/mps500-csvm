/**
 */
package de.tu_bs.cs.isf.sparql;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Prefix</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see de.tu_bs.cs.isf.sparql.SparqlPackage#getPrefix()
 * @model
 * @generated
 */
public interface Prefix extends Entry {
} // Prefix
