/**
 */
package de.tu_bs.cs.isf.sparql;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Insert Block</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see de.tu_bs.cs.isf.sparql.SparqlPackage#getInsertBlock()
 * @model
 * @generated
 */
public interface InsertBlock extends Block {
} // InsertBlock
