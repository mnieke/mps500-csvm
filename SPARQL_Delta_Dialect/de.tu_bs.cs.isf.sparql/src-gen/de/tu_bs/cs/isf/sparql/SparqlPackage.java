/**
 */
package de.tu_bs.cs.isf.sparql;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see de.tu_bs.cs.isf.sparql.SparqlFactory
 * @model kind="package"
 * @generated
 */
public interface SparqlPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "sparql";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://www.tu-braunschweig.de/isf/sparql/1.0";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "sparql";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	SparqlPackage eINSTANCE = de.tu_bs.cs.isf.sparql.impl.SparqlPackageImpl.init();

	/**
	 * The meta object id for the '{@link de.tu_bs.cs.isf.sparql.impl.SparqlQueryImpl <em>Query</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.tu_bs.cs.isf.sparql.impl.SparqlQueryImpl
	 * @see de.tu_bs.cs.isf.sparql.impl.SparqlPackageImpl#getSparqlQuery()
	 * @generated
	 */
	int SPARQL_QUERY = 0;

	/**
	 * The feature id for the '<em><b>Prefixes</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SPARQL_QUERY__PREFIXES = 0;

	/**
	 * The feature id for the '<em><b>Blocks</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SPARQL_QUERY__BLOCKS = 1;

	/**
	 * The number of structural features of the '<em>Query</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SPARQL_QUERY_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Query</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SPARQL_QUERY_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link de.tu_bs.cs.isf.sparql.impl.EntryImpl <em>Entry</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.tu_bs.cs.isf.sparql.impl.EntryImpl
	 * @see de.tu_bs.cs.isf.sparql.impl.SparqlPackageImpl#getEntry()
	 * @generated
	 */
	int ENTRY = 1;

	/**
	 * The feature id for the '<em><b>Text</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENTRY__TEXT = 0;

	/**
	 * The number of structural features of the '<em>Entry</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENTRY_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>Entry</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENTRY_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link de.tu_bs.cs.isf.sparql.impl.PrefixImpl <em>Prefix</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.tu_bs.cs.isf.sparql.impl.PrefixImpl
	 * @see de.tu_bs.cs.isf.sparql.impl.SparqlPackageImpl#getPrefix()
	 * @generated
	 */
	int PREFIX = 2;

	/**
	 * The feature id for the '<em><b>Text</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PREFIX__TEXT = ENTRY__TEXT;

	/**
	 * The number of structural features of the '<em>Prefix</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PREFIX_FEATURE_COUNT = ENTRY_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Prefix</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PREFIX_OPERATION_COUNT = ENTRY_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link de.tu_bs.cs.isf.sparql.impl.BlockImpl <em>Block</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.tu_bs.cs.isf.sparql.impl.BlockImpl
	 * @see de.tu_bs.cs.isf.sparql.impl.SparqlPackageImpl#getBlock()
	 * @generated
	 */
	int BLOCK = 3;

	/**
	 * The feature id for the '<em><b>Entries</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLOCK__ENTRIES = 0;

	/**
	 * The number of structural features of the '<em>Block</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLOCK_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>Block</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BLOCK_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link de.tu_bs.cs.isf.sparql.impl.InsertBlockImpl <em>Insert Block</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.tu_bs.cs.isf.sparql.impl.InsertBlockImpl
	 * @see de.tu_bs.cs.isf.sparql.impl.SparqlPackageImpl#getInsertBlock()
	 * @generated
	 */
	int INSERT_BLOCK = 4;

	/**
	 * The feature id for the '<em><b>Entries</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INSERT_BLOCK__ENTRIES = BLOCK__ENTRIES;

	/**
	 * The number of structural features of the '<em>Insert Block</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INSERT_BLOCK_FEATURE_COUNT = BLOCK_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Insert Block</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INSERT_BLOCK_OPERATION_COUNT = BLOCK_OPERATION_COUNT + 0;


	/**
	 * Returns the meta object for class '{@link de.tu_bs.cs.isf.sparql.SparqlQuery <em>Query</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Query</em>'.
	 * @see de.tu_bs.cs.isf.sparql.SparqlQuery
	 * @generated
	 */
	EClass getSparqlQuery();

	/**
	 * Returns the meta object for the reference list '{@link de.tu_bs.cs.isf.sparql.SparqlQuery#getPrefixes <em>Prefixes</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Prefixes</em>'.
	 * @see de.tu_bs.cs.isf.sparql.SparqlQuery#getPrefixes()
	 * @see #getSparqlQuery()
	 * @generated
	 */
	EReference getSparqlQuery_Prefixes();

	/**
	 * Returns the meta object for the reference list '{@link de.tu_bs.cs.isf.sparql.SparqlQuery#getBlocks <em>Blocks</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Blocks</em>'.
	 * @see de.tu_bs.cs.isf.sparql.SparqlQuery#getBlocks()
	 * @see #getSparqlQuery()
	 * @generated
	 */
	EReference getSparqlQuery_Blocks();

	/**
	 * Returns the meta object for class '{@link de.tu_bs.cs.isf.sparql.Entry <em>Entry</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Entry</em>'.
	 * @see de.tu_bs.cs.isf.sparql.Entry
	 * @generated
	 */
	EClass getEntry();

	/**
	 * Returns the meta object for the attribute '{@link de.tu_bs.cs.isf.sparql.Entry#getText <em>Text</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Text</em>'.
	 * @see de.tu_bs.cs.isf.sparql.Entry#getText()
	 * @see #getEntry()
	 * @generated
	 */
	EAttribute getEntry_Text();

	/**
	 * Returns the meta object for class '{@link de.tu_bs.cs.isf.sparql.Prefix <em>Prefix</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Prefix</em>'.
	 * @see de.tu_bs.cs.isf.sparql.Prefix
	 * @generated
	 */
	EClass getPrefix();

	/**
	 * Returns the meta object for class '{@link de.tu_bs.cs.isf.sparql.Block <em>Block</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Block</em>'.
	 * @see de.tu_bs.cs.isf.sparql.Block
	 * @generated
	 */
	EClass getBlock();

	/**
	 * Returns the meta object for the reference list '{@link de.tu_bs.cs.isf.sparql.Block#getEntries <em>Entries</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Entries</em>'.
	 * @see de.tu_bs.cs.isf.sparql.Block#getEntries()
	 * @see #getBlock()
	 * @generated
	 */
	EReference getBlock_Entries();

	/**
	 * Returns the meta object for class '{@link de.tu_bs.cs.isf.sparql.InsertBlock <em>Insert Block</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Insert Block</em>'.
	 * @see de.tu_bs.cs.isf.sparql.InsertBlock
	 * @generated
	 */
	EClass getInsertBlock();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	SparqlFactory getSparqlFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link de.tu_bs.cs.isf.sparql.impl.SparqlQueryImpl <em>Query</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.tu_bs.cs.isf.sparql.impl.SparqlQueryImpl
		 * @see de.tu_bs.cs.isf.sparql.impl.SparqlPackageImpl#getSparqlQuery()
		 * @generated
		 */
		EClass SPARQL_QUERY = eINSTANCE.getSparqlQuery();

		/**
		 * The meta object literal for the '<em><b>Prefixes</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SPARQL_QUERY__PREFIXES = eINSTANCE.getSparqlQuery_Prefixes();

		/**
		 * The meta object literal for the '<em><b>Blocks</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SPARQL_QUERY__BLOCKS = eINSTANCE.getSparqlQuery_Blocks();

		/**
		 * The meta object literal for the '{@link de.tu_bs.cs.isf.sparql.impl.EntryImpl <em>Entry</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.tu_bs.cs.isf.sparql.impl.EntryImpl
		 * @see de.tu_bs.cs.isf.sparql.impl.SparqlPackageImpl#getEntry()
		 * @generated
		 */
		EClass ENTRY = eINSTANCE.getEntry();

		/**
		 * The meta object literal for the '<em><b>Text</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ENTRY__TEXT = eINSTANCE.getEntry_Text();

		/**
		 * The meta object literal for the '{@link de.tu_bs.cs.isf.sparql.impl.PrefixImpl <em>Prefix</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.tu_bs.cs.isf.sparql.impl.PrefixImpl
		 * @see de.tu_bs.cs.isf.sparql.impl.SparqlPackageImpl#getPrefix()
		 * @generated
		 */
		EClass PREFIX = eINSTANCE.getPrefix();

		/**
		 * The meta object literal for the '{@link de.tu_bs.cs.isf.sparql.impl.BlockImpl <em>Block</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.tu_bs.cs.isf.sparql.impl.BlockImpl
		 * @see de.tu_bs.cs.isf.sparql.impl.SparqlPackageImpl#getBlock()
		 * @generated
		 */
		EClass BLOCK = eINSTANCE.getBlock();

		/**
		 * The meta object literal for the '<em><b>Entries</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BLOCK__ENTRIES = eINSTANCE.getBlock_Entries();

		/**
		 * The meta object literal for the '{@link de.tu_bs.cs.isf.sparql.impl.InsertBlockImpl <em>Insert Block</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.tu_bs.cs.isf.sparql.impl.InsertBlockImpl
		 * @see de.tu_bs.cs.isf.sparql.impl.SparqlPackageImpl#getInsertBlock()
		 * @generated
		 */
		EClass INSERT_BLOCK = eINSTANCE.getInsertBlock();

	}

} //SparqlPackage
