/**
 */
package de.tu_bs.cs.isf.sparql.impl;

import de.tu_bs.cs.isf.sparql.*;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class SparqlFactoryImpl extends EFactoryImpl implements SparqlFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static SparqlFactory init() {
		try {
			SparqlFactory theSparqlFactory = (SparqlFactory)EPackage.Registry.INSTANCE.getEFactory(SparqlPackage.eNS_URI);
			if (theSparqlFactory != null) {
				return theSparqlFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new SparqlFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SparqlFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case SparqlPackage.SPARQL_QUERY: return createSparqlQuery();
			case SparqlPackage.ENTRY: return createEntry();
			case SparqlPackage.PREFIX: return createPrefix();
			case SparqlPackage.INSERT_BLOCK: return createInsertBlock();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SparqlQuery createSparqlQuery() {
		SparqlQueryImpl sparqlQuery = new SparqlQueryImpl();
		return sparqlQuery;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Entry createEntry() {
		EntryImpl entry = new EntryImpl();
		return entry;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Prefix createPrefix() {
		PrefixImpl prefix = new PrefixImpl();
		return prefix;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public InsertBlock createInsertBlock() {
		InsertBlockImpl insertBlock = new InsertBlockImpl();
		return insertBlock;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SparqlPackage getSparqlPackage() {
		return (SparqlPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static SparqlPackage getPackage() {
		return SparqlPackage.eINSTANCE;
	}

} //SparqlFactoryImpl
