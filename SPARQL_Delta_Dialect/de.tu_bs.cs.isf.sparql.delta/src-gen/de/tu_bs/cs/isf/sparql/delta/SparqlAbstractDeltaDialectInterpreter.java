package de.tu_bs.cs.isf.sparql.delta;

import java.util.List;

import org.deltaecore.core.decore.DEArgument;
import org.deltaecore.core.decore.DEDeltaOperationCall;
import org.deltaecore.core.decoredialect.DEDeltaOperationDefinition;
import org.deltaecore.core.variant.interpretation.DEDeltaDialectInterpreter;

import org.deltaecore.core.variant.interpretation.locking.DEModelWriter;
import de.tu_bs.cs.isf.sparql.Block;
import de.tu_bs.cs.isf.sparql.SparqlQuery;
import de.tu_bs.cs.isf.sparql.Prefix;
import de.tu_bs.cs.isf.sparql.Entry;

public abstract class SparqlAbstractDeltaDialectInterpreter implements DEDeltaDialectInterpreter {
	
	@Override
	public boolean interpretDeltaOperationCall(DEDeltaOperationCall deltaOperationCall, DEModelWriter modelWriter) {
		DEDeltaOperationDefinition deltaOperationDefinition = deltaOperationCall.getOperationDefinition();
		String deltaOperationName = deltaOperationDefinition.getName();
		
		List<DEArgument> arguments = deltaOperationCall.getArguments();
			
		if (deltaOperationName.equals("addBlockToBlocksOfSparqlQuery")) {
			Block value = (Block) arguments.get(0).getExpression().getValue();
			SparqlQuery element = (SparqlQuery) arguments.get(1).getExpression().getValue();			

			return interpretAddBlockToBlocksOfSparqlQuery(modelWriter, value, element);
		}
			
		if (deltaOperationName.equals("addPrefixToPrefixesOfSparqlQuery")) {
			Prefix value = (Prefix) arguments.get(0).getExpression().getValue();
			SparqlQuery element = (SparqlQuery) arguments.get(1).getExpression().getValue();			

			return interpretAddPrefixToPrefixesOfSparqlQuery(modelWriter, value, element);
		}
			
		if (deltaOperationName.equals("addEntryToEntriesOfBlock")) {
			Entry value = (Entry) arguments.get(0).getExpression().getValue();
			Block element = (Block) arguments.get(1).getExpression().getValue();			

			return interpretAddEntryToEntriesOfBlock(modelWriter, value, element);
		}
			
		if (deltaOperationName.equals("modifyTextOfEntry")) {
			String value = (String) arguments.get(0).getExpression().getValue();
			Entry element = (Entry) arguments.get(1).getExpression().getValue();			

			return interpretModifyTextOfEntry(modelWriter, value, element);
		}
			
		if (deltaOperationName.equals("modifyTextOfPrefix")) {
			String value = (String) arguments.get(0).getExpression().getValue();
			Prefix element = (Prefix) arguments.get(1).getExpression().getValue();			

			return interpretModifyTextOfPrefix(modelWriter, value, element);
		}
			
		if (deltaOperationName.equals("writeSparqlFile")) {
			String filePath = (String) arguments.get(0).getExpression().getValue();
			SparqlQuery query = (SparqlQuery) arguments.get(1).getExpression().getValue();			

			return interpretWriteSparqlFile(modelWriter, filePath, query);
		}
		
		return false;
	}


	protected boolean interpretAddBlockToBlocksOfSparqlQuery(DEModelWriter modelWriter, Block value, SparqlQuery element) {
		//Add value to element.blocks
		modelWriter.addValue(element, 1, value);
		return true;
	}

	protected boolean interpretAddPrefixToPrefixesOfSparqlQuery(DEModelWriter modelWriter, Prefix value, SparqlQuery element) {
		//Add value to element.prefixes
		modelWriter.addValue(element, 0, value);
		return true;
	}

	protected boolean interpretAddEntryToEntriesOfBlock(DEModelWriter modelWriter, Entry value, Block element) {
		//Add value to element.entries
		modelWriter.addValue(element, 0, value);
		return true;
	}

	protected boolean interpretModifyTextOfEntry(DEModelWriter modelWriter, String value, Entry element) {
		//Modify the value of element.text to have the value of value
		modelWriter.setValue(element, 0, value);
		return true;
	}

	protected boolean interpretModifyTextOfPrefix(DEModelWriter modelWriter, String value, Prefix element) {
		//Modify the value of element.text to have the value of value
		modelWriter.setValue(element, 0, value);
		return true;
	}

	abstract protected boolean interpretWriteSparqlFile(DEModelWriter modelWriter, String filePath, SparqlQuery query);
}
