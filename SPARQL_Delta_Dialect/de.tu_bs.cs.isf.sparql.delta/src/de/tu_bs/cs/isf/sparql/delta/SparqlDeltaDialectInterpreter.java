package de.tu_bs.cs.isf.sparql.delta;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;

import org.deltaecore.core.variant.interpretation.locking.DEModelWriter;

import de.tu_bs.cs.isf.sparql.Block;
import de.tu_bs.cs.isf.sparql.Entry;
import de.tu_bs.cs.isf.sparql.InsertBlock;
import de.tu_bs.cs.isf.sparql.Prefix;
import de.tu_bs.cs.isf.sparql.SparqlQuery;

//This class is generated only once and will NOT be overwritten. Changed abstract methods of the base class have to be implemented manually.
public class SparqlDeltaDialectInterpreter extends SparqlAbstractDeltaDialectInterpreter {

	private static final String PREFIX_PREFIX = "PREFIX ";
	
	private static final String INSERT_DATA_PREFIX = "INSERT DATA { ";
	
	@Override
	protected boolean interpretWriteSparqlFile(DEModelWriter modelWriter, String filePath, SparqlQuery query) {
		
		if(query == null) {
			return false;
		}
		
		File file = new File(filePath);
		if(file.exists()) {
			// TODO do something? Prompt to overwrite? -> not sensible for headless...think about it.
			return true;
		}
		
		try {
			file.createNewFile();
			
			Writer fileWriter = new FileWriter(file);
			
			for(Prefix prefix: query.getPrefixes()) {
				fileWriter.write(PREFIX_PREFIX);
				
				fileWriter.write(prefix.getText());
				
				fileWriter.write(System.lineSeparator());
			}
			
			for(Block block: query.getBlocks()) {
				if(block instanceof InsertBlock) {
					fileWriter.write(INSERT_DATA_PREFIX);
					
					for(Entry entry: block.getEntries()) {
						fileWriter.write(entry.getText());
						
						fileWriter.write(System.lineSeparator());
					}
					
					fileWriter.write("}");
				}
			}
			
			
			fileWriter.close();
			
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return true;
	}
}