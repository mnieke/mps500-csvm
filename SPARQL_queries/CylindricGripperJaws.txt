INSERT { GRAPH <http://newConfig> {
          ?subject ?predicate ?object;
          a owl:NamedIndividual.}
      } 
WHERE {
          BIND(IRI(STR("http://MIM19#CylindricGripperJaws")) AS ?subject).
          
BIND(IRI(STR("http://www.w3.org/1999/02/22-rdf-syntax-ns#type")) AS ?predicate).
          
BIND(IRI(STR("http://www.hsu-ifa.de/ontologies/VDI2206#Component")) AS ?object).
      }
      ;

INSERT { GRAPH <http://newConfig> {
          ?subject ?predicate ?object;
          a owl:NamedIndividual.}
      } 
WHERE {
          BIND(IRI(STR("http://MIM19#PneumaticGripper")) AS ?subject).
          
BIND(IRI(STR("http://www.hsu-ifa.de/ontologies/VDI2206#ModuleConsistsOfComponent")) AS ?predicate).
          
BIND(IRI(STR("http://MIM19#CylindricGripperJaws")) AS ?object).
      }
;

# DINEN61360_INSERT_SIMPLE with UUID bindings
# Necessary W3C ontologies
PREFIX owl: <http://www.w3.org/2002/07/owl#>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>

# Necessary SemAnz40 standard ontologies
PREFIX DE6: <http://www.hsu-ifa.de/ontologies/DINEN61360#>

INSERT {GRAPH <http://newConfig> {
    # Declaration of individuals
  ?DE_uuid rdf:type DE6:Data_Element;													# Definition of Data Element (DE)
      a owl:NamedIndividual;
      rdfs:label ?DE_Label.
  ?DE_Instance_uuid rdf:type DE6:Instance_Description;								# Definition of DEÂ´s Instance Description
      a owl:NamedIndividual;
      rdfs:label ?DEI_Label.

    
    # Declaration of ObjectProperties
    ?DE_uuid DE6:has_Instance_Description ?DE_Instance_uuid;								# OP b from DE to type and instance
      DE6:has_Type_Description ?DE_TypeIRI. 
    ?DE_TypeIRI DE6:Type_Description_has_Instance ?DE_Instance_uuid.						# OP from type to instance and data type
              
      # Defines the individual to connect the data element to
      ?DescribedIndividualIRI DE6:has_Data_Element ?DE_uuid.
          
    # Declaration of DataProperties for the DEÂ´s Instance description
    ?DE_Instance_uuid DE6:Expression_Goal ?Expression_Goal;								# Enumeration {"Actual_Value" , "Assurance" , "Requirement"}
      DE6:Logic_Interpretation ?Logic_Interpretation;										# Enumeration {"<" , "<=" , "=" , ">" , ">="}
      DE6:Value ?Value;
    DE6:Created_at_Date ?Created_at_Date.  									            # Time stamp format  CCYY-MM-DDThh:mm:ss
                                
 } 
} WHERE {

  { SELECT 
      #Individual that is described by the data element
      ?DescribedIndividualIRI

      # uuids for data element, data element instance, data element data type
      ?DE_uuid ?DE_Instance_uuid 

      # Data Element type
      ?DE_TypeIRI

      # Labels
      ?DE_Label ?DEI_Label

      # data element instance description attributes
      ?Expression_Goal ?Logic_Interpretation ?Created_at_Date

      # value
      ?Value

  WHERE
    {    
      # CHANGES TO THIS PART ALWAYS REQUIRED

      # Define the individual to be described by the data element
      BIND(IRI(STR("http://MIM19#CylindricGripperJaws")) AS ?DescribedIndividualIRI).
     
      # Define the data element instance attributes
      BIND(STR("Actual_Value") AS ?Expression_Goal).
      BIND(STR("=") AS ?Logic_Interpretation).
  
      # Define the value to use
      BIND(STR("60") AS ?Value).
      
      BIND(STR("GripperJawsOpeningWidth") AS ?Code).
      ?DE_TypeIRI DE6:Code ?Code;
      rdfs:label ?DET_Label.

      # ----------------------------------------------------------------- #
      # NO CHANGES TO THIS PART EVER
      # Defines the general namespace for all individuals
      BIND(STR("http://MIM19#") AS ?NameSpace).
      BIND(CONCAT(?DET_Label,"_Data_Element") AS ?DE_Label).
      BIND(CONCAT(?DET_Label,"_Instance") AS ?DEI_Label).
      BIND(NOW() AS ?Created_at_Date).
      BIND(UUID() AS ?DE_uuid).
      BIND(UUID() AS ?DE_Instance_uuid).
    }
  }

}
;
