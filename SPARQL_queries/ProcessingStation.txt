
INSERT { GRAPH <http://newConfig>{
          ?subject ?predicate ?object;
          a owl:NamedIndividual.}
      } 
WHERE {
          BIND(IRI(STR("http://MIM19#ProcessingStation")) AS ?subject).
          
BIND(IRI(STR("http://www.w3.org/1999/02/22-rdf-syntax-ns#type")) AS ?predicate).
          
BIND(IRI(STR("http://www.hsu-ifa.de/ontologies/VDI2206#System")) AS ?object).
      }
;
INSERT { GRAPH <http://newConfig> {
          ?subject ?predicate ?object;
          a owl:NamedIndividual.}
      } 
WHERE {
          BIND(IRI(STR("http://MIM19#DrillingModule")) AS ?subject).
          
BIND(IRI(STR("http://www.hsu-ifa.de/ontologies/VDI2206#SystemConsistsOfSystem")) AS ?predicate).
          
BIND(IRI(STR("http://MIM19#ProcessingStation")) AS ?object).
      }
;
